TARGET=pses2019

all: pdf

pdf:
	rm -f $(TARGET).pdfpc
	pdflatex $(TARGET).tex
	# iconv -f iso-8859-1 -t utf-8 $(TARGET).pdfpc -o tmp.pdfpc
	# mv tmp.pdfpc $(TARGET).pdfpc

final: pdf
	pdfunite slides_florent.pdf $(TARGET).pdf $(TARGET)_merged.pdf
	./cpdf -scale-to-fit "381mm 286mm" $(TARGET)_merged.pdf -o $(TARGET)_final.pdf
	rm $(TARGET)_merged.pdf
	# mv $(TARGET).pdfpc $(TARGET)_final.pdfpc
	# sed -i -e 's/$(TARGET).pdf/$(TARGET)_final.pdf/g' $(TARGET)_final.pdfpc


run:
	pdfpc $(TARGET)_final.pdfpc

clean:
	rm -f *.dvi *.aux *.bbl *.blg $(TARGET).ps $(TARGET).pdf *.toc *.ind *.out *.brf *.ilg *.idx *.log *.bcf *.nav *.run.xml *.snm *.vrb *.backup tex/*.backup *~

mrproper: clean
	rm -f $(TARGET).pdf $(TARGET)_final.pdf $(TARGET).pdfpc $(TARGET)_final.pdfpc
